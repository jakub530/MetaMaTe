from db_access import DbAccess
from base64 import b64encode
from set_user_id import Id


class Picture(object):
    """ Get the picture of a person in base64 """
    def __init__(self):
        self.user_id = Id().user_id
        # Database details
        self.server = 'KNOX'
        self.database = 'employee'


        self.picture_data = self.get_picture_data()

    def get_picture_data(self):
        sql_query = 'SELECT * FROM employee.dbo.photo where person_id={}'\
                    .format(self.user_id)

        results = DbAccess(self.server, self.database, self.user, self.password)\
                  .query_db_no_params(sql_query)

        return b64encode(results[0][2])

if __name__ == '__main__':
    Picture()

