from datetime import datetime, timedelta


class DateRange(object):
    """ Makes date range class for a space of one week. """
    def __init__(self):
        self.start_date = datetime.today()
        self.end_date = self.start_date + timedelta(days=7)

        self.start_date_str = self.start_date.strftime('%d-%m-%Y')
        self.end_date_str = self.end_date.strftime('%d-%m-%Y')
        self.start_date_sql = self.start_date.strftime('%Y%m%d')
        self.end_date_sql = self.end_date.strftime('%Y%m%d')

if __name__ == '__main__':
    # Small test
    print DateRange().start_date_str
    print DateRange().end_date_str
