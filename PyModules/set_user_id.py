from db_access import DbAccess
from constants import *

class Id(object):
    """ Get the user_id of the person. """
    def __init__(self):
        # Database details
        self.server = 'KNOX'
        self.database = 'employee'
        self.user = 'canteen_app'
        self.password = '35p-Roll'

        self.user_id = self.get_user_id()

    def get_user_id(self):
        sql_query = "SELECT person_id FROM employee.dbo.personnel where initials = '{}'"\
                    .format(USERNAME)

        results = DbAccess(self.server, self.database, self.user, self.password)\
                  .query_db_no_params(sql_query)

        return results[0][0]

if __name__ == '__main__':
    Id()
