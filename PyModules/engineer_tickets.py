from db_access import DbAccess


class EngineerTickets(object):

    def __init__(self, initials):

        self.initials = initials.upper()

        # These are the database details used to connect to the Issue system.
        self.server = 'JAWS3'
        self.database = 'msg'
        self.user = 'public_sfr_ro'
        self.password = 'Psro2354LEhsx'

        self.tickets = self.query_issue_system()

    def query_issue_system(self):
        """
        Query the issues system. Get all open, non-blocked issues of interest.
        """
        sql_query = "EXEC query.qry_issues "
        sql_query += "@field_list=?, "
        sql_query += "@filter=?"

        field_list = "issue_id," \
                     "customername," \
                     "status," \
                     "owner," \
                     "internal_owner"

        filter_list = "(owner = '{0}' or internal_owner = '{0}') and " \
                      "status = 'open' and " \
                      "blocked = 'No'"\
                      .format(self.initials)

        param_list = [field_list, filter_list]

        return DbAccess(self.server, self.database, self.user, self.password)\
              .query_db_params(sql_query, param_list, zipped=True)

if __name__ == '__main__':
    print EngineerTickets('HAB').tickets
