import pyodbc

class DbAccess(object):
    """Class providing an API for querying any database."""
    def __init__(self, server, database, user, password):
        """
        We initialise with parameters describing the database and the way in
        which we should query it.

        :param server: The name of the server
        :param database: The name of the database.
        :param user: The username for the database.
        :param password: The corresponding password.

        """
        # Made as global objects for logging purposes.
        self.server = server
        self.database = database

        # We create our string for accessing the database using pyodbc.
        self.connect_str = ("DRIVER=SQL Server; SERVER={}; UID={}; PWD={}; "
                            "APP=PythonScript; DATABASE={};".
                            format(server, user, password, database))

    def query_db_no_params(self, sql_query, zipped=False):
        """
        Execute a query on a given DB and return the set of results.
        """
        # We initiate a connection to the database.
        with pyodbc.connect(self.connect_str) as connection:
            # Instantiate a cursor so we can run SQL queries
            cursor = connection.cursor()
            cursor.execute(sql_query)

            # We have been passed a flag to zip the results so we create a dict
            # zipping each of the column names with the query results.
            return self._zip(cursor) if zipped else cursor.fetchall()

    def query_db_params(self, sql_query, query_params, zipped=False):
        """
        Execute a query on a given DB and return the set of results.
        """
        # We initiate a connection to the database.
        with pyodbc.connect(self.connect_str) as connection:
            # Instantiate a cursor so we can run SQL queries
            cursor = connection.cursor()
            cursor.execute(sql_query, query_params)

            # We have been passed a flag to zip the results so we create a dict
            # zipping each of the column names with the query results.
            return self._zip(cursor) if zipped else cursor.fetchall()

    def query_db_recursive_fetch(self, sql_query, query_params):
        """
        Execute a query on a given DB and recursively fetch the result set
        returning all sets of a results as a list of a list of pyodbc.Row.

        :param sql_query: SQL query to execute.
        :param query_params: Parameters to insert into SQL query.
        :return: List of list of pyodbc.Row, one list of each set of results.
        """
        # We initiate a connection to the database.
        with pyodbc.connect(self.connect_str) as connection:
            connection.timeout = 480
            # Instantiate a cursor so we can run SQL queries
            cursor = connection.cursor()
            cursor.execute(sql_query, query_params)

            # Now we recursively try pulling data from the database until there
            # is no more sets of data to pull.
            # At this point we return all the data as a list.
            try:
                data = []
                while True:
                    data.append(cursor.fetchall())
                    if not cursor.nextset():
                        break
                return data
            except pyodbc.ProgrammingError, e:
                raise pyodbc.ProgrammingError(
                    "Failed to get all results of the query."
                    "It's possible there were no results to fetch.\n", e)

    def _zip(self, cursor):
        """ Fetches the results and zips the values with the corresponding
            column titles.
        """
        # We want to output the information as a list of dicts, e.g.
        # [{'issue_id': 12345, ...}, ...]. This isn't a built in feature of
        # pyodbc, and so we follow Bryan's answer at
        # http://stackoverflow.com/questions/16519385/output-pyodbc-cursor-
        # results-as-python-dictionary
        # cursor.description returns a list of tuples with 0th entry as
        # column name, which we then use as keys.
        columns = [column[0] for column in cursor.description]
        # List to add to.
        rows = []
        for row in cursor.fetchall():
            # The zip function returns a list of tuples made up of the nth
            # elements of columns and the row, pairing the column title with
            # the information contained in that column. This is then turned
            # into a dict and appended to rows.
            row_dict = dict(zip(columns, row))
            rows.append(row_dict)

        return rows
