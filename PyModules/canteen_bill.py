import re


def get_bill():
    regex = r'<span id="lblNewBalance">(?P<bill>[0-9]+.[0-9]{2})<\/span>'

    with open(r'C:\Users\es3\Documents\MetaMaid\PyModules\bill.html', 'r') as f:
        file = f.read().replace('\n', '')
        matches = []
        for match in re.finditer(regex, file):
            amount = match.groupdict()['bill']
            matches.append(amount)
        latest_bill = matches[-1]
        return latest_bill

if __name__ == '__main__':
    print get_bill()
