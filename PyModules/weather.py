import feedparser
import pprint
from collections import namedtuple
from unidecode import unidecode
from constants import *


class Weather(object):
    """ Makes date range class for a space of one week. """
    def __init__(self, link):
        self.link = link

        self.weather = self.create_output()

    def create_output(self):
        feed = feedparser.parse(self.link)
        string = ""
        string +="["
        for entry in feed["entries"]:
            description = ""
            Max_Temperature_position = 21 + entry["title"].index("Maxi")
            Min_Temperature_position = 21 + entry["title"].index("Mini")
            start_position = 0
            end_position = 0
            start = 2
            letter_position = 0 
            Min_Temperature = entry["title"][Min_Temperature_position:Min_Temperature_position+2]
            Max_Temperature= entry["title"][Max_Temperature_position:Max_Temperature_position+2]
       
            for day in days_of_week:
                if day in unidecode(entry["title"]):
                    Today = day
            for letter in unidecode(entry["title"]):
                if start>0:
                    if(letter == ":"):
                        start_position = letter_position +2
                        start = start -1
                    if(letter == ","):
                        end_position = letter_position
                        start= start -1
                letter_position +=1 
            description = unidecode(entry["title"])[start_position:end_position]

            
            string += "{"
            string += '"Day":"{0}"'.format(Today)
            string += ","
            string += '"Description":"{0}"'.format(description)
            string += ","
            string += '"Max_Temperature":"{0}"'.format(Max_Temperature)
            string += ","
            string += '"Min_Temperature":"{0}"'.format(Min_Temperature)


            if(entry == feed["entries"][-1]):
                string+="}"
            else:
                string+="},"

        string += "]"
        return string



if __name__ =='__main__':
    weather = Weather(weather_link)
    print weather.weather
