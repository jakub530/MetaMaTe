import json
from db_access import DbAccess
from date_range import DateRange
from constants import *


class Whereabouts(object):
    def __init__(self, initials):
        dates = DateRange()

        self.day_status = []

        server = 'KNOX'
        database = 'employee'
        user = 'public_directory_ro'
        password = 'Pdro6232HFfsdk'

        self.initials_db = DbAccess(server, database, user, password)
        self.payroll_db = DbAccess(server, database, user, password)
        whereabouts = self._get_whereabouts('{}'.format(initials),
                                            dates.start_date_sql,
                                            dates.end_date_sql)

        # ACCESS USING day_status ONLY.
        self.day_status = self.to_json(whereabouts)

    def _get_whereabouts(self, initials, start_date, end_date):

        # First get the engineer's payroll ID.
        payroll_id = self._get_payroll_id(initials)

        # Build the SQL query
        sql_query = "EXEC employee.public_ro.where_qry_emp "
        sql_query += "@payroll=?, "
        sql_query += "@start_date=?, "
        sql_query += "@end_date=?"

        # Build the parameter list
        params = [payroll_id, start_date, end_date]

        # Run the query, zip the results.
        return self.payroll_db.query_db_params(sql_query, params, zipped=True)

    def _get_payroll_id(self, initials):

        sql_query = "EXEC employee.public_ro.qry_directory_entry "
        sql_query += "@initials=?"

        # We need to a pass a list of params.
        params = [initials]
        query_result = self.initials_db.query_db_params(sql_query,
                                                        params,
                                                        zipped=True)
        # with open('test.txt', 'w'):
        #     for i in query_result:

        # The query results are returned in a list of dicts, which we need to
        # convert to a string.
        payroll_id = str(query_result[0]['payroll'])
        return payroll_id

    def to_json(self, whereabouts):
        day_status = []

        for half_day in whereabouts:
            in_or_out = half_day['state_abbrev'].upper()
            day = half_day['wrhd_start_date'].strftime('%A')

            state = 'IN' if in_or_out in IN_REASONS \
                else 'OUT - {}'.format(half_day['combined_detail'])

            updated = False
            for status in day_status:
                if status['day'] == day:
                    status['Afternoon'] = state
                    updated = True

            if not updated:
                day_status.append({'day': day, 'Morning': state})

        return day_status

if __name__ == '__main__':
    w = Whereabouts('HYS')
    print w.day_status
