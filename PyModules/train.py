import feedparser
import pprint
from collections import namedtuple
from unidecode import unidecode
train_website = "http://ojp.nationalrail.co.uk/service/ldbboard/dep/ENF"
list_of_digits = [str(k) for k in range(0,10)]

class feed(object):
    def __init__(self, link):
        self.link = link
    def create_output(self):
        feed = feedparser.parse(self.link)
        character_number = 0
        hour = ""
        string = ""
        list_of_hours = []
        for characters in feed["feed"]["summary"]:

            if characters == ":":
                if feed["feed"]["summary"][character_number-1] in list_of_digits:
                    hour = ""
                    hour+=feed["feed"]["summary"][character_number-2]
                    hour+=feed["feed"]["summary"][character_number-1]
                    hour+=characters
                    hour+=feed["feed"]["summary"][character_number+1]
                    hour+=feed["feed"]["summary"][character_number+2]
                    if (hour not in list_of_hours):
                        list_of_hours.append(unidecode(hour))


            character_number += 1
        list_of_hours.pop()
        string +="["
        for hour in list_of_hours:
            string += "{"
           # string += "\n"
            string += '"Train_Station":"London Liverpool Street"'
            string += ","
            string += '"Time":"{0}"'.format(hour)
            #string += "\n"
            if(hour == list_of_hours[-1]):
                string+="}"
            else:
                string+="},"
        string+="]"
        return string 




if __name__ =='__main__':
    train_times = feed(train_website)
    print train_times.create_output()
