from subprocess import call
from random import randint
from math import log, floor
import webbrowser
from xml.dom import minidom
import re
import sre_constants

from pytagcloud import create_tag_image, make_tags
from pytagcloud.lang.counter import get_tag_counts


class ChromeWordle(object):
    def __init__(self):
        # Make the history file.
        call('.\ChromeHistoryView.exe /sxml history.xml')

        self.process_xml()

    def process_xml(self):
        # Only match on HTTP addresses, skips chrome extensions.
        # Catches accesses to sfr
        regex_website = r'http(s)?:\/\/(www\.)?(?P<link>[a-z]+)\.'
        try:
            regex = re.compile(regex_website)
        except sre_constants, e:
            print e
            raise

        root = minidom.parse('history.xml')
        root = root.getElementsByTagName('chrome_visited_sites')[0]
        items = root.getElementsByTagName('item')

        links = {}

        for item in items:
            url = item.getElementsByTagName('url')[0]
            url = url.firstChild.nodeValue
            match = re.match(regex, url)
            if match:
                # This is a website we care about. It's a real site.
                link = match.groupdict()['link']
                if link in links:
                    links[link] += 1
                else:
                    links[link] = 1

        new_links = ''
        for link, count in links.items():
            choice = randint(0, 2)
            if not choice:
                link += ' '
                link *= int(floor(log(count)))
                new_links += link

        self._make_wordle(str(new_links))

    def _make_wordle(self, links):
        try:
            tags = make_tags(get_tag_counts(links), maxsize=100)
            create_tag_image(tags, './app/wordle.png', size=(900, 600),
                             fontname='Lobster')
        except Exception, e:
            print e

if __name__ == '__main__':
    ChromeWordle()


