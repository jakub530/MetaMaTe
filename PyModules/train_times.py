import feedparser
from unidecode import unidecode
from constants import *
list_of_digits = [str(k) for k in range(0,10)]


class Train(object):
    def __init__(self, link):
        self.link = link

        self.train_times = self.create_output()

    def create_output(self):
        feed = feedparser.parse(self.link)
        character_number = 0
        string = ""
        list_of_hours = []
        for characters in feed["feed"]["summary"]:

            if characters == ":":
                if feed["feed"]["summary"][character_number-1] in list_of_digits:
                    hour = feed["feed"]["summary"][character_number-2]
                    hour += feed["feed"]["summary"][character_number-1]
                    hour += characters
                    hour += feed["feed"]["summary"][character_number+1]
                    hour += feed["feed"]["summary"][character_number+2]
                    if hour not in list_of_hours:
                        list_of_hours.append(unidecode(hour))

            character_number += 1

        list_of_hours.pop()

        string +="["
        for hour in list_of_hours:
            string += "{"
            string += '"Train_Station": "London Liverpool Street", '
            string += '"Time": "{0}"'.format(hour)
            string += "}" if hour == list_of_hours[-1] else "}, "
        string += "]"

        return string 


if __name__ == '__main__':
    train_times = Train(train_website)
    print train_times.train_times
