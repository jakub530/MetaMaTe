import subprocess
# File used for storing constants

USERNAME = subprocess.check_output('echo %username%', shell=True) \
           .split()[0].upper()

# Reasons/Places where workers are not working
OUT_REASONS = ['',
               'TRAVEL',
               'MATERNTY',
               'SICK',
               'OFF-PT',
               'HOLIDAY',
               'S12',
               'BANKHOL',
               'JURY',
               'MORALE',
               'SABBATCL',
               'OUT',
               'OFF-SHF'
               ]

# Reasons/Places for workers to be working
IN_REASONS = ['IN',
              'HIGHGATE',
              'CAMBRIDGE',
              'CHESTER',
              'DENVER',
              'RESTON',
              'OSLO',
              'COVENTRY',
              'RICHARDSON',
              'HOME',
              'SF',
              'PALOALTO',
              'DALLAS',
              'BOCA',
              'ENFIELD',
              'EDINBURGH',
              'CUSTOMER',
              'COURSE',
              'CONFRNCE'
              ]

weather_link = "http://open.live.bbc.co.uk/weather/feeds/en/2643743/3dayforecast.rss"
days_of_week = ["Monday","Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]

big_names = ["Google", "Bing", "Youtube", "9gag", "Imgur", "Facebook",
             "Twitter", "Cdaction", "Metaswitch", "STKit",
             "Wikipedia", "Github", "StackOverflow", "Amazon", "BBC", "eBay",
             "plot.ly", "Slack", "Chrome", "Django",
             "Meta.Docs", "Docs.Python"]

bbc_news_link = "http://feeds.bbci.co.uk/news/rss.xml"
economic_link = "http://www.economist.com/sections/business-finance/rss.xml"
reddit_link = "https://www.reddit.com/.rss"
train_website = "http://ojp.nationalrail.co.uk/service/ldbboard/dep/ENF"