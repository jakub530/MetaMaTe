import feedparser
import pprint
from collections import namedtuple
from unidecode import unidecode
from constants import *


class Feed(object):
    """ Makes date range class for a space of one week. """
    def __init__(self, link):
        self.link = link

        self.feeds = self.create_output()
    
    def create_output(self):
        feed = feedparser.parse(self.link)
        string = ""
        news_number = 0
        news_list = []
        News = namedtuple("News", "title link" )
        for row in feed["entries"]:
            if news_number < 3:
                if self.link == reddit_link or self.link == economic_link:
                    news = News(unidecode(row["title"]), row["links"][0]["href"])
                else:
                    news = News(unidecode(row["summary_detail"]["value"]), row["links"][0]["href"])
                news_list.append(news)
                news_number += 1

        string += "["
        for entry in news_list:
            string += "{"
            temporary = str(entry.title)
            temporary = temporary.replace('"', " ")
            string += '"Title":"{0}", '.format(temporary)
            string += '"Link": "{0}"'.format(entry.link)
            string += "}" if entry == news_list[-1] else "},"
        string += "]"

        return string

if __name__ == '__main__':
    economic = Feed(bbc_news_link)
    print economic.feeds