import datetime
import win32com.client
from datetime import timedelta, datetime
from collections import namedtuple
from unidecode import unidecode


class DateRange(object):
    """ Makes date range class for a space of one week. """
    def __init__(self):
        self.start_date = datetime.today()
        self.end_date = self.start_date + timedelta(days=7)
        self.start_date_str = self.start_date.strftime('%d-%m-%Y')
        self.end_date_str = self.end_date.strftime('%d-%m-%Y')
        self.start_date_sql = self.start_date.strftime('%Y%m%d')
        self.end_date_sql = self.end_date.strftime('%Y%m%d')


Event = namedtuple("Event", "start_time end_time title owner" )
Message = namedtuple("Message", "subject time sender")


class Outlook(object):
    def __init__(self):
        self.messages = self.import_messages()
        self.events = self.import_events()    

    def import_messages(self):
        messages_output = []
        outlook = win32com.client.Dispatch("Outlook.Application").GetNamespace("MAPI")
        
        inbox = outlook.GetDefaultFolder(6) # "6" refers to the index of a folder - in this case,
                                            # the inbox. You can change that number to reference
                                            # any other folder
        messages = inbox.Items

        for message in messages:
            if(message.Importance>1):
                Time_str = str(message.ReceivedTime)
                Time = datetime.strptime(Time_str, '%m/%d/%y %H:%M:%S')
                if(Time >datetime.today() - timedelta(days = 14)):
                    new_message = Message(unidecode(message.subject), message.ReceivedTime, message.SenderName)
                    messages_output.append(new_message)
    
        with open('outlook_email_output', 'w') as file:
            string = ""
            string+= "["
            for entry in messages_output:
              
                string += "{"
                string += ""
                string += '"Subject":"{0}"'.format(entry.subject)
                string += ","
                string += '"Time":"{0}"'.format(entry.time)
                string += ","
                string += '"Sender":"{0}"'.format(entry.sender)
               # string += "\n"
                if(entry == messages_output[-1]):
                    string+="}"
                else:
                    string+="},"
    
               # string += "\n"  
                file.write("{")
                file.write("\n")
                file.write('"Subject":"{0}"'.format(entry.subject))
                file.write("\n")
                file.write('"Time":"{0}"'.format(entry.time))
                file.write("\n")
                file.write("}")
                file.write("\n")
            string += "]"
     #   print string
        return string
        
    def import_events(self):
    
        output = []
        Outlook = win32com.client.Dispatch("Outlook.Application")
        ns = Outlook.GetNamespace("MAPI")
        
        appointments = ns.GetDefaultFolder(9).Items 
        appointments.Sort("[Start]")
        begin = datetime.today()
       # print begin
        end = begin + timedelta(days = 30);
      #  print end
        restriction = "[Start] >= '" + begin.strftime("%m/%d/%Y") + "' AND [End] <= '" +end.strftime("%m/%d/%Y") + "'"
        restrictedItems = appointments.Restrict(restriction)
        
        for appointmentItem in appointments:
     
            #print("{0} Start: {1}, End: {2}, Organizer: {3}".format(
            #     unidecode(appointmentItem.Subject), appointmentItem.Start, 
            #  appointmentItem.End, unidecode(appointmentItem.Organizer)))
            event = Event(str(appointmentItem.Start), str(appointmentItem.End), unidecode(appointmentItem.Subject), unidecode(appointmentItem.Organizer))
            output.append(event)
        next_week_events = []
        for row in output:
        
            date_object_start = datetime.strptime(row.start_time, '%m/%d/%y %H:%M:%S')
            date_object_end = datetime.strptime(row.end_time, '%m/%d/%y %H:%M:%S')
            date_object_range_end = datetime.strptime(DateRange().end_date_str, '%d-%m-%Y')
            date_object_range_start = datetime.strptime(DateRange().start_date_str, '%d-%m-%Y')
        
            if(date_object_start > date_object_range_start and date_object_range_end> date_object_end):
                next_week_events.append(row)
      #  pp = pprint.PrettyPrinter(indent=4)
      #  pp.pprint(next_week_events)
        with open('outlook_calendar_output', 'w') as file:
            string = ""
            string += "["
            for entry in next_week_events:
                string += "{"
                #string += "\n"
                string += '"Topic":"{0}"'.format(entry.title)
                string += ","
                string += '"Start_Time":"{0}"'.format(entry.start_time)
                string += ","
                string += '"End_Time":"{0}"'.format(entry.end_time)
                string += ","
                string += '"Organizer":"{0}"'.format(entry.owner)
               # string += "\n"
                if(entry == next_week_events[-1]):
                    string+="}"
                else:
                    string+="},"
               # string += "\n"  
    
                file.write("{")
                file.write("\n")
                file.write('"Topic":"{0}"'.format(entry.title))
                file.write("\n")
                file.write('"Start_Time":"{0}"'.format(entry.start_time))
                file.write("\n")
                file.write('"End_Time":"{0}"'.format(entry.end_time))
                file.write("\n")
                file.write('"Organizer":"{0}"'.format(entry.owner))
                file.write("\n") 
                file.write("}")
            string +="]"
            file.write("\n")
        return string
    
if __name__ =='__main__':
    print Outlook().messages
    print type(Outlook().events)
