import datetime
import os
import distance
import chart_plotter
import shutil

big_names = ["Google", "Bing", "Youtube", "9gag", "Imgur", "Facebook", "Twitter", "Cdaction", "Metaswitch", "STKit",
             "Wikipedia", "Github", "StackOverflow", "Amazon", "BBC", "eBay", "plot.ly", "Slack", "Chrome", "Django",
             "Meta.Docs", "Docs.Python"]


class DataReader(object):

    def __init__(self, path, file_name):
        self.path = path
        self.file_name = file_name

    def run_chrome_history_view(self):
        """
        Run the .exe which exists at path, and export the output into a file
        :return: the file handle
        """
        path = self.path
        file_name = self.file_name

        #os.system("cd %s" % path)
        os.system(".\ChromeHistoryView.exe /stext test.txt /sort 2")
        my_file = open(file_name, mode='r')
        lines = my_file.readlines()
        my_file.close()
        return lines


class DataAnalyser(object):

    def __init__(self, lines):
        self.lines = lines

    def input_analyse(self):

        lines = self.lines
        web_visit_dict = {}
        last_visit_time = None
        cur_url = None

        for line in lines:
            line = line.replace('\x00', '')
            if str.startswith(line, "URL"):
                string_list = line.split()
                cur_url = string_list[2]
            elif str.startswith(line, "Visited On"):
                string_list = line.split()
                date_object = datetime.datetime.strptime((string_list[3]+' '+string_list[4]), '%d/%m/%Y %H:%M:%S')
                if last_visit_time is not None:
                    # if datetime.datetime.now() - date_object < datetime.timedelta(days=3):
                        duration = date_object - last_visit_time
                        if duration < datetime.timedelta(seconds=10):
                            continue
                        for name in big_names:
                            if name.lower() in cur_url.lower():
                                if name not in web_visit_dict:
                                    web_visit_dict.update(
                                        {name: duration}
                                    )
                                else:
                                    web_visit_dict[name] += duration
                                break
                        else:
                            if 'login.live' in cur_url:
                                name = 'Microsoft'
                                if name not in web_visit_dict:
                                    web_visit_dict.update(
                                        {name: duration}
                                    )
                                else:
                                    web_visit_dict[name] += duration
                            elif 'col130.mail' in cur_url or 'outlook' in cur_url or 'gmail' in cur_url:
                                name = 'Microsoft Email'
                                if name not in web_visit_dict:
                                    web_visit_dict.update(
                                        {name: duration}
                                    )
                                else:
                                    web_visit_dict[name] += duration
                            else:
                                cur_url = cur_url.replace('https://', '')
                                cur_url = cur_url.replace('http://', '')
                                cur_url = cur_url.replace('www', '')
                                cur_url = cur_url.replace('/', ' ')
                                cur_url = cur_url.replace('.', ' ')
                                cur_url = cur_url.replace('com', '')
                                cur_url = cur_url.replace('org', '')
                                cur_url = cur_url.replace('co uk', '')
                                cur_url = cur_url.replace('us', '')
                                for i in range(5):
                                    cur_url = cur_url.replace('  ', ' ')
                                if len(cur_url) > 30:
                                    cur_url = cur_url[:30]
                                counter = 0
                                for key, value in web_visit_dict.iteritems():
                                    if counter > 10:
                                        break
                                    elif distance.levenshtein(key, cur_url) < 5:
                                        web_visit_dict[key] += duration
                                        break
                                    counter += 1
                                else:
                                    web_visit_dict.update(
                                        {cur_url: duration}
                                    )
                last_visit_time = date_object

        my_cp = chart_plotter.ChartPlotter({'Time Usage': web_visit_dict})
        my_cp.bar()
        my_cp.pie()

        return web_visit_dict

def time_pictures():
    mydr = DataReader('.\PyModules', 'test.txt')
    my_lines = mydr.run_chrome_history_view()
    myda = DataAnalyser(my_lines)
    myda.input_analyse()
    os.system("del test.txt")
    shutil.move("Time Usage Bar Chart.png", "./app/Time_Usage_Bar.png")
    shutil.move("Time Usage Pie Chart.png", "./app/Time_Usage_Pie.png")

if __name__ == '__main__':
    time_pictures()