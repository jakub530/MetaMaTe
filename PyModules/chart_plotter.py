import plotly.plotly as py
import plotly.graph_objs as go
import operator


class PlotError(Exception):
    """
    This defines the Exception where the plot fails
    """

    def __str__(self):
        return "Sorry, but your plot enquiry fails"


class ChartPlotter(object):
    """
    This class defines a chart plotter which plots all the data collected into
    several bar charts, which takes a dictionary of dictionary
    """

    def __init__(self, data_to_plot={}):
        """
        :param data_to_plot: the dictionary of data used to plot the chart,
        which is in the format of {'type':{'website':duration,...},...}
        """
        self.data_to_plot = data_to_plot

    def bar(self):
        """
        Plot a bar chart
        :raise PlotError: The data is not in the right format
        """

        # title stores the title of the chart
        # web_dic stores the dictionaries
        for title, web_dic in self.data_to_plot.iteritems():

            # web_list stores the website list
            # dur_list stores the duration list
            web_list = []
            dur_list = []
            total_dur = 0.0
            web_dic = sorted(web_dic.iteritems(),
                             key=operator.itemgetter(1))
            for website, duration in web_dic:
                total_dur += duration.seconds
            for website, duration in web_dic:
                web_list.append(website)
                dur_list.append(duration.seconds/total_dur*100.0)

            other_time = 0.0
            i = len(dur_list) - 1
            while i >= 0:
                if dur_list[i] < 0.105:
                    other_time += dur_list[i]
                    del web_list[i]
                    del dur_list[i]
                i -= 1

            if other_time > 0.1:
                web_list.append('Others')
                dur_list.append(other_time)

            for i in range(len(dur_list)):
                dur_list[i] = round(dur_list[i], 1)

            trace = go.Bar(
                x=web_list, y=dur_list, marker=dict(
                    color='rgb(158,202,225)',
                    line=dict(
                        color='rgb(8,48,107)',
                        width=1.5),
                ),
                opacity=0.6
            )
            data = [trace]
            layout = go.Layout(title=title + ' Percentage Chart', xaxis=dict(tickangle=-45),
                               annotations=[dict(x=xi, y=yi, text=str(yi)+'%',
                                                 xanchor='center',
                                                 yanchor='bottom',
                                                 showarrow=False,
                                                 )
                                            for xi, yi in zip(web_list,
                                                              dur_list)])
            fig = go.Figure(data=data, layout=layout)

            py.image.save_as(fig, '%s Bar Chart.png' % title)

    def pie(self):
        """
        Plot a pie chart
        """

        data_list = []

        for title, web_dic in self.data_to_plot.iteritems():

            # web_list stores the website list
            # dur_list stores the duration list
            web_list = []
            dur_list = []
            total_dur = 0.0
            web_dic = sorted(web_dic.iteritems(),
                             key=operator.itemgetter(1))
            for website, duration in web_dic:
                total_dur += duration.seconds
            for website, duration in web_dic:
                web_list.append(website)
                dur_list.append(duration.seconds / total_dur * 100.0)

            other_time = 0.0
            i = len(dur_list) - 1
            while i >= 0:
                if dur_list[i] < 0.105:
                    other_time += dur_list[i]
                    del web_list[i]
                    del dur_list[i]
                i -= 1

            if other_time > 0.1:
                web_list.append('Others')
                dur_list.append(other_time)

            for i in range(len(dur_list)):
                dur_list[i] = round(dur_list[i], 1)

            my_data = {
                "values": dur_list,
                "labels": web_list,
                "name": title,
                "hoverinfo": "label+percent+name",
                "hole": .4,
                "type": "pie"
            }

            data_list.append(my_data)

        fig = {
            "data": data_list,
            "layout": {
                "title": "%s Percentage Chart" % title,
            }
        }
        py.image.save_as(fig, '%s Pie Chart.png' % title)
