﻿import {Component,  trigger, state,style,animate,transition} from '@angular/core'
import {DataService} from './data.service'
import {UserComponent} from './components/user-login.component'
import {ModuleSelector} from './moduleSelector'
import {HomeComponent} from './components/home.component'
// Component meta-data
@Component({
    selector: "my-device",
    templateUrl: "app/app.html",
    directives: [ UserComponent, HomeComponent],
    providers: [DataService],
    animations: [
        trigger('moduleState', [
            state('inactive', style({ transform: 'translateX(0) scale(1)' })),
            state('active', style({ transform: 'translateX(110%) scale(1.3)' })),
            transition('inactive => active', animate('200ms ease-in')),
            transition('active => inactive', animate('100ms ease-out')),
            transition('void => inactive', [
                style({ transform: 'translateX(-110%) scale(1)' }),
                animate('250ms linear')
            ]),
            transition('inactive => void', [
                animate('250ms linear', style({ transform: 'translateX(100%) scale(1)' }))
            ]),
            transition('void => active', [
                style({ transform: 'translateX(0) scale(0)' }),
                animate('250ms linear')
            ]),
            transition('active => void', [
                animate('250ms linear', style({ transform: 'translateX(0) scale(0)' }))
            ])
        ])
    ]
})

export class AppComponent{
    title = 'MetaMate';
    constructor(
        private dataService: DataService) { }
    private userSelected: boolean = false;
     browser = new ModuleSelector(name = 'browser');
     where = new ModuleSelector(name = 'where');
     meeting = new ModuleSelector(name = 'meeting');
     weather = new ModuleSelector(name = 'weather');
     bill = new ModuleSelector(name = 'bill');
     tickets = new ModuleSelector(name = 'tickets');
     bbc = new ModuleSelector(name = 'bbc');
     reddit = new ModuleSelector(name = 'reddit');
     messages = new ModuleSelector(name = 'messages');
     train = new ModuleSelector(name = 'train');

    onUser(user) {
        this.userSelected = true;
    }
}