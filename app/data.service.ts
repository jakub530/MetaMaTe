﻿import { Injectable, Inject  } from '@angular/core';
import { Http, Response, Headers, RequestOptions} from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/share';
import {User} from './user';

// Service that encompasses all app interaction with the controllers and database
// This includes get/post/put functionality in all components
// Uses @Injectable decorator instead of @Component
@Injectable()
export class DataService {

   
    // Variable declarations for User GET
    private users: Array<User> = new Array<User>();
    public user_collection$: Observable<Array<User>>;
    private user_collectionObserver: any;
    private user_collection: Array<User>;

    // Variable declarations for Photo GET
    private photo: string;


    // Initialize instances of required Arrays/Observables, as well as Angular2 Http
    constructor( @Inject(Http) private http: Http) {
        this.http = http;


        this.user_collection = new Array<User>();
        this.user_collection$ = new Observable <User[]>(observer => {
            this.user_collectionObserver = observer;
        }).share();

        
    }

    // Function to GET PurchasingLocation List
   

    // Function to GET List of User Names and Ids for log-in w/autocomplete
    getUser() {
        this.http.get('api/user').map((res: Response) => res.json())
            .subscribe((users: Array<User>) => {
                this.user_collectionObserver.next(users);
            });
    }

    // Function that PUTs a purchaser Id and returns the corresponding photo data as base64 data
    getPhoto(purchaserId: number): Observable<Response>{
        let headers = new Headers({ 'Content-Type': 'application/json;' });
        let options = new RequestOptions({ headers: headers });
        return this.http.put('api/photo',purchaserId,options);
    }
    
    
  

}