﻿//Type for User data

export interface User {
    firstName : string;
    initials: string;
    purchaserId: number;
    lastName: string;
}