﻿export class ModuleSelector {
    constructor(public name: string,
        public state = 'inactive') {
    }

    toggleState() {
        this.state = (this.state === 'active' ? 'inactive' : 'active');
    }
}

