import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response} from '@angular/http';
import {Tickets} from './tickets'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "tickets",
    templateUrl: "app/components/home-content/tickets.html",
    directives: [],
    providers: []
})

export class TicketsComponent implements OnInit {
    constructor(public http: Http) {
    }

    public ticketsData: Array<Tickets> = new Array<Tickets>();

    ngOnInit() {
        this.getTickets();
    }

    getTickets() {
        this.http.get('http://localhost:8888/tickets')
            .subscribe(
            data => this.ticketsData = JSON.parse(data.text()));
    }

}