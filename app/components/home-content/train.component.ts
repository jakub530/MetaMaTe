import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response} from '@angular/http';
import {Train} from './train'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "train",
    templateUrl: "app/components/home-content/train.html",
    directives: [],
    providers: []
})

export class TrainComponent implements OnInit {
    constructor(public http: Http) {
    }

    public trainData: Array<Train> = new Array<Train>();

    ngOnInit() {
        this.getTrain();
    }

    getTrain() {
        this.http.get('http://localhost:8888/train_times')
            .subscribe(
            data => this.trainData = JSON.parse(data.text()));
    }

}