import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response} from '@angular/http';
import {Bbc} from './bbc'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "bbc",
    templateUrl: "app/components/home-content/bbc.html",
    directives: [],
    providers: []
})

export class BbcComponent implements OnInit {
    constructor(public http: Http) {
    }

    public bbcData: Array<Bbc> = new Array<Bbc>();

    ngOnInit() {
        this.getBbc();
    }

    getBbc() {
        this.http.get('http://localhost:8888/news')
            .subscribe(
            data => this.bbcData = JSON.parse(data.text()));
    }

}