﻿import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response } from '@angular/http';
import {Weather} from './weather'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "weather",
    templateUrl: "app/components/home-content/weather.html",
    directives: [],
    providers: []
})

export class WeatherComponent implements OnInit {
    constructor(public http: Http) {
    }

    public weatherData: Array<Weather> = new Array<Weather>();

    ngOnInit() {
        this.getWeather();
    }

    getWeather() {
        this.http.get('http://localhost:8888/weather')
            .subscribe(
            data => this.weatherData = JSON.parse(data.text()));
    }

}