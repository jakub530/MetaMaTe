﻿import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers ,Response } from '@angular/http';
import {Whereabouts} from './whereabouts'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
   selector: "where",
   templateUrl: "app/components/home-content/where.html",
   directives: [],
   providers: []
})

export class WhereComponent implements OnInit{
   constructor(public http: Http) {
 }

   public whereData: Array<Whereabouts> = new Array <Whereabouts>();

ngOnInit(){   
this.getWhere();
}

getWhere() {
       this.http.get('http://localhost:8888/where')
    .subscribe(
    data=>this.whereData=JSON.parse(data.text()));
   }

}