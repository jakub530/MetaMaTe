export interface Weather {
    Day: string;
    Description: string;
    Max_Temperature: string;
    Min_Temperature: string;
}
