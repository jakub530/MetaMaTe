export interface Messages {
    Subject: string;
    Time: string;
    Sender: string;
}
