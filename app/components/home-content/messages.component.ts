import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response} from '@angular/http';
import {Messages} from './messages'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "messages",
    templateUrl: "app/components/home-content/messages.html",
    directives: [],
    providers: []
})

export class MessagesComponent implements OnInit {
    constructor(public http: Http) {
    }

    public messagesData: Array<Messages> = new Array<Messages>();

    ngOnInit() {
        this.getMessages();
    }

    getMessages() {
        this.http.get('http://localhost:8888/messages')
            .subscribe(
            data => this.messagesData = JSON.parse(data.text()));
    }

}