export interface Meeting {
    Topic: string;
    Start_Time: string;
    End_Time: string;
    Organizer: string;
}
