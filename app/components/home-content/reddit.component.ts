import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response} from '@angular/http';
import {Reddit} from './reddit'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "reddit",
    templateUrl: "app/components/home-content/reddit.html",
    directives: [],
    providers: []
})

export class RedditComponent implements OnInit {
    constructor(public http: Http) {
    }

    public redditData: Array<Reddit> = new Array<Reddit>();

    ngOnInit() {
        this.getReddit();
    }

    getReddit() {
        this.http.get('http://localhost:8888/reddit')
            .subscribe(
            data => this.redditData = JSON.parse(data.text()));
    }

}