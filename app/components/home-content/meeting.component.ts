﻿import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response } from '@angular/http';
import {Meeting} from './meeting'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "meeting",
    templateUrl: "app/components/home-content/meeting.html",
    directives: [],
    providers: []
})

export class MeetingComponent implements OnInit {
    constructor(public http: Http) {
    }

    public meetingData: Array<Meeting> = new Array<Meeting>();

    ngOnInit() {
        this.getMeeting();
    }

    getMeeting() {
        this.http.get('http://localhost:8888/meeting')
            .subscribe(
            data => this.meetingData = JSON.parse(data.text()));
    }

}