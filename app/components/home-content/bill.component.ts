﻿import {Component, OnInit} from '@angular/core'
import { Http, HTTP_PROVIDERS, Headers, Response } from '@angular/http';
import {Whereabouts} from './whereabouts'
import {Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/share'

// Component meta-data
@Component({
    selector: "bill",
    templateUrl: "app/components/home-content/bill.html",
    directives: [],
    providers: []
})

export class BillComponent implements OnInit {
    constructor(public http: Http) {
    }

    public billData: string;

    ngOnInit() {
        this.getBill();
    }

    getBill() {
        this.http.get('http://localhost:8888/bill')
            .subscribe(
            data => this.billData = data.text());
    }

}