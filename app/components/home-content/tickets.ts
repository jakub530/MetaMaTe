export interface Tickets {
    status: string;
    owner: string;
    issue_id: string;
    customername: string;
    internal_owner: string;
}
