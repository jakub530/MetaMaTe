﻿import {Component, Input, OnChanges, SimpleChange, trigger,state, style, animate,transition} from '@angular/core'
import {BrowserComponent} from './home-content/browser.component'
import {WhereComponent} from './home-content/where.component'
import {MeetingComponent} from './home-content/meeting.component'
import {WeatherComponent} from './home-content/weather.component'
import {BillComponent} from './home-content/bill.component'

import {TicketsComponent} from './home-content/tickets.component'
import {BbcComponent} from './home-content/bbc.component'
import {RedditComponent} from './home-content/reddit.component'
import {MessagesComponent} from './home-content/messages.component'
import {TrainComponent} from './home-content/train.component'


// Component meta-data
@Component({
    selector: "home",
    templateUrl: "app/components/home.html",
    directives: [BrowserComponent, WhereComponent, MeetingComponent, BillComponent, WeatherComponent, TicketsComponent, BbcComponent, RedditComponent, MessagesComponent, TrainComponent],
    providers: [],
    animations: [
        trigger('flyInOut', [
            state('in', style({ transform: 'translateX(0)' })),
            transition('void => *', [
                style({ transform: 'translateX(-100%)' }),
                animate('150ms linear')
            ]),
            transition('* => void', [
                animate('150ms linear', style({ transform: 'translateX(-100%)' }))
            ])
        ])
    ]
})

export class HomeComponent implements OnChanges{
    @Input() browser: string;
    @Input() where: string;
    @Input() meeting: string;
    @Input() weather: string;
    @Input() bill: string;
    @Input() bbc: string;
    @Input() reddit: string;
    @Input() tickets: string;
    @Input() messages: string;
    @Input() train: string;

    private chosenModules = [];

    ngOnChanges(changes: { [propkey: string]: SimpleChange }) {

        for (let propName in changes) {
            let changedProp = changes[propName];
            let from = JSON.stringify(changedProp.previousValue);
            let to = JSON.stringify(changedProp.currentValue);

            if (changedProp.currentValue == 'active') {
                this.chosenModules.push(propName);
                console.log(this.chosenModules);
            }
            if (changedProp.currentValue == 'inactive') {
                for (let modules of this.chosenModules) {
                    if (modules == propName) {
                        this.chosenModules.splice(this.chosenModules.indexOf(modules), 1);
                    }
                }
            }
        }
    }
}