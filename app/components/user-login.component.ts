﻿import {Component, Inject, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core'

import { Http, HTTP_PROVIDERS, Headers, Response } from '@angular/http';
import {DomSanitizationService, SafeUrl } from '@angular/platform-browser'
import {DataService} from '../data.service'
import {User} from '../user'

//User Login Component
//
// Fetches list of user names and initials
// Holds AutoComplete directive
// Fetches photo once user is selected

@Component({
    selector: "user-login",
    templateUrl: "app/components/user-login.html",
    directives: [],
})
export class UserComponent implements OnInit{


    // IMPORTANT. SafeUrl type must be used for sanitization command
    private photo: SafeUrl;

    constructor(
    private sanitizer:DomSanitizationService,
    public http: Http) {
    }

    // Initialize instance of dataservice
    ngOnInit() {
        this.fetchPhoto();
    }

    // On userSelect, set selected user, emit selected user, fetch photo using purchaser ID
    // IMPORTANT. Diff syntax used for callback function (edge case to reference execution context)
    // Required because user-selected is called by [value-changed] in html, which exists in the
    // external AutoComplete directive
    // Compare to other event triggers (eg. in buttonpadcomponent) to see differences

    // IMPORTANT. Sanitizer imported as workaround for angular2's automatic cross-site scripting blocks. 
    // Without bypassSecurityTrustUrl, 'unsafe' tag is added to URL
    fetchPhoto() {
     this.http.get('http://localhost:8888/photo')
            .subscribe(res => this.photo = this.sanitizer.bypassSecurityTrustUrl('data:image / jpg; base64,'+ res.text()));
        
    }


   
}