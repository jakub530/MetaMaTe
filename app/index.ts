﻿// Initializes Angular2 app and bootstrap, with HTTP_Providers
// If routing was included in this app, ROUTER_PROVIDERS would be required
import { bootstrap }      from '@angular/platform-browser-dynamic'
import { HTTP_PROVIDERS } from '@angular/http'
import { AppComponent }         from './app.component';

bootstrap(AppComponent, [
    HTTP_PROVIDERS
]);
