from datetime import timedelta
from flask import make_response, request, current_app
from functools import update_wrapper
from flask import Flask
from flask_cors import CORS, cross_origin
from PyModules import whereabouts, engineer_tickets, outlook, \
                      set_user_id, canteen_bill, \
                      data_reader, chrome_history
from PyModules.user_picture import Picture
from PyModules.train_times import Train
from PyModules.news import Feed
from PyModules.weather import Weather
from PyModules.constants import *

app = Flask(__name__)


def crossdomain(origin=None, methods=None, headers=None,
                max_age=21600, attach_to_all=True,
                automatic_options=True):
    if methods is not None:
        methods = ', '.join(sorted(x.upper() for x in methods))
    if headers is not None and not isinstance(headers, basestring):
        headers = ', '.join(x.upper() for x in headers)
    if not isinstance(origin, basestring):
        origin = ', '.join(origin)
    if isinstance(max_age, timedelta):
        max_age = max_age.total_seconds()

    def get_methods():
        if methods is not None:
            return methods

        options_resp = current_app.make_default_options_response()
        return options_resp.headers['allow']

    def decorator(f):
        def wrapped_function(*args, **kwargs):
            if automatic_options and request.method == 'OPTIONS':
                resp = current_app.make_default_options_response()
            else:
                resp = make_response(f(*args, **kwargs))
            if not attach_to_all and request.method != 'OPTIONS':
                return resp

            h = resp.headers

            h['Access-Control-Allow-Origin'] = origin
            h['Access-Control-Allow-Methods'] = get_methods()
            h['Access-Control-Max-Age'] = str(max_age)
            if headers is not None:
                h['Access-Control-Allow-Headers'] = headers
            return resp

        f.provide_automatic_options = False
        return update_wrapper(wrapped_function, f)
    return decorator

def make_pictures():
    data_reader.time_pictures()
    chrome_history.ChromeWordle()

def get_user_picture():
    pic = Picture()
    return pic.picture_data

def get_events():
    events = outlook.Outlook()
    return events.events

def get_messages():
    messages = outlook.Outlook()
    return messages.messages.replace('\t', ' ')

def get_bill():
    return canteen_bill.get_bill()

def get_bbc():
    link = Feed(bbc_news_link)
    return link.feeds

def get_reddit():
    link = Feed(reddit_link)
    return link.feeds

def get_train_times():
    times = Train(train_website)
    return times.train_times

def get_weather():
    weather_data = Weather(weather_link)
    return weather_data.weather

@app.route('/photo')
@cross_origin()
def photo():
    return get_user_picture()

@app.route('/where')
@cross_origin()
def where():
    where = whereabouts.Whereabouts('ES3')
    return str(where.day_status).replace("'", '"')

@app.route('/meeting')
@cross_origin()
def meeting():
    return get_events()

@app.route('/weather')
@cross_origin()
def weather():
    return get_weather()

@app.route('/bill')
@cross_origin()
def bill():
    return get_bill()

@app.route('/tickets')
@cross_origin()
def tickets():
    # We can use USERNAME but 'ES3' doesn't have any tickets
    tickets = engineer_tickets.EngineerTickets('HAB')
    return str(tickets.tickets).replace("'", '"')

@app.route('/news')
@cross_origin()
def news():
    return get_bbc()

@app.route('/reddit')
@cross_origin()
def reddit():
    return get_reddit()

@app.route('/messages')
@cross_origin()
def messages():
    return get_messages()


@app.route('/train_times')
@cross_origin()
def train_times():
    return get_train_times()


if __name__ == '__main__':
    set_user_id.Id()
    #make_pictures()
    print "Done making images!"
    app.run('localhost', port=8888, debug=True)
